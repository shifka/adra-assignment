import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UploadComponent } from './components/upload/upload.component';
import { ReportComponent } from './components/report/report.component';
import { BalancesComponent } from './components/balances/balances.component';
import { LoginComponent } from './components/login/login.component';
import { BaseComponent } from './components/base/base.component';
import { AuthGuard } from './helpers/auth.guard';


const routes: Routes = [
  { path: 'upload', component: BaseComponent, canActivate: [AuthGuard] },
  { path: 'report', component: BaseComponent, canActivate: [AuthGuard]  },
  { path: 'balances', component: BaseComponent, canActivate: [AuthGuard]  },
  { path: 'login', component: LoginComponent },
  { path: '**', redirectTo: 'balances' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
