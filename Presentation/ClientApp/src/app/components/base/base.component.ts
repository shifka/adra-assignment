import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-base',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.scss']
})
export class BaseComponent implements OnInit {

  selectedIndex = 0;
  userRole: string;
  constructor(private router: Router,
              private authenticationService: AuthService) {
    const path = router.routerState.snapshot.url;
    switch (path) {
      case '/balances': { this.selectedIndex = 0; break; }
      case '/upload': { this.selectedIndex = 1; break; }
      case '/report': { this.selectedIndex = 2; break; }
      default: { this.selectedIndex = 0; break; }
    }

    this.userRole = this.authenticationService.getLoggedUserRole();
  }

  ngOnInit() {

  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }

}
