import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Moment } from 'moment';
import * as _moment from 'moment';
import { MatDatepicker, DateAdapter, MAT_DATE_LOCALE, MAT_DATE_FORMATS, MatSnackBar } from '@angular/material';
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { HttpRequest, HttpClient, HttpEventType } from '@angular/common/http';
import { environment } from 'src/environments/environment';

const moment = _moment;

export const DATE_FORMAT = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    { provide: MAT_DATE_FORMATS, useValue: DATE_FORMAT },
  ],
})
export class UploadComponent implements OnInit {

  formGroup: FormGroup;
  private progress = 0;

  constructor(private formBuilder: FormBuilder, private http: HttpClient, private _snackBar: MatSnackBar) { }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.formGroup = this.formBuilder.group({
      date: [moment(), Validators.required],
      file: [null, Validators.required]
    });
  }

  chosenYearHandler(normalizedYear: Moment) {
    const ctrlValue = this.formGroup.get('date').value;
    ctrlValue.year(normalizedYear.year());
    this.formGroup.get('date').setValue(ctrlValue);
  }

  chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.formGroup.get('date').value;
    ctrlValue.month(normalizedMonth.month());
    this.formGroup.get('date').setValue(ctrlValue);
    datepicker.close();
  }

  uploadDocument(files: any) {

    const selectedDate = this.formGroup.get('date').value;
    if (!files || files.length === 0 || !selectedDate) {
      return;
    }

    const formData = new FormData();
    // tslint:disable-next-line: prefer-for-of
    for (let i = 0; i < files.length; i++) {
      formData.append('files', files[i]);
    }
    const date = selectedDate.clone().startOf('month');
    formData.append('date', moment(date._d).format('YYYY-MM-DD'));

    const uploadReq = new HttpRequest('POST', environment.api_base_url + `balance/upload/`, formData, {
      reportProgress: true,
    });

    this.http.request(uploadReq).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        this.progress = Math.round(100 * event.loaded / event.total);
      } else if (event.type === HttpEventType.Response || event.type === HttpEventType.ResponseHeader) {
        if (event.ok) {
          console.log('Uploaded');
          this.openSnackBar("File uploaded successfully", "Success");
        }
        else {
          this.openSnackBar("There was an error uploading file. Please check the format", "Error");
        }
      }
    });
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }

}
