import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DataService } from 'src/app/services/data.service';
import { Moment } from 'moment';
import * as _moment from 'moment';
import { MatDatepicker, DateAdapter, MAT_DATE_LOCALE, MAT_DATE_FORMATS } from '@angular/material';
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';

const moment = _moment;

export const DATE_FORMAT = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    { provide: MAT_DATE_FORMATS, useValue: DATE_FORMAT },
  ],
})
export class ReportComponent implements OnInit {

  public view: any[] = [700, 400];
  public showXAxis = true;
  public showYAxis = true;
  public gradient = false;
  public showLegend = true;
  public showXAxisLabel = true;
  public xAxisLabel: 'Months';
  public showYAxisLabel = true;
  public yAxisLabel: 'Amount';
  public autoScale = true;
  public colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  formGroup: FormGroup;
  dataSource = [];

  constructor(private formBuilder: FormBuilder, private dataService: DataService) { }

  ngOnInit() {
    this.createForm();
    this.loadBalances();
  }

  createForm() {
    this.formGroup = this.formBuilder.group({
      from: [moment(), Validators.required],
      to: [moment(), Validators.required]
    });
  }

  chosenYearHandler(field: string, normalizedYear: Moment) {
    const ctrlValue = this.formGroup.get(field).value;
    ctrlValue.year(normalizedYear.year());
    this.formGroup.get(field).setValue(ctrlValue);
  }

  chosenMonthHandler(field: string, normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.formGroup.get(field).value;
    ctrlValue.month(normalizedMonth.month());
    this.formGroup.get(field).setValue(ctrlValue);
    datepicker.close();
    this.loadBalances();
  }

  loadBalances() {
    const selectedFromDate = this.formGroup.get('from').value;
    const selectedToDate = this.formGroup.get('to').value;
    const from = selectedFromDate.clone().startOf('month');
    const to = selectedToDate.clone().startOf('month');
    const req = {
      from: moment(from._d).format('YYYY-MM-DD'),
      to: moment(to._d).format('YYYY-MM-DD')
    };
    console.log(req);
    this.dataService.getBalancesForPeriod(req).then(res => {
      if (res) {
        this.dataSource = res;
      }
    });
  }

}
