import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Moment } from 'moment';
import * as _moment from 'moment';
import { MatDatepicker, DateAdapter, MAT_DATE_LOCALE, MAT_DATE_FORMATS } from '@angular/material';
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { AuthService } from 'src/app/services/auth.service';

const moment = _moment;

export const DATE_FORMAT = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-balances',
  templateUrl: './balances.component.html',
  styleUrls: ['./balances.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    { provide: MAT_DATE_FORMATS, useValue: DATE_FORMAT },
  ],
})

export class BalancesComponent implements OnInit {

  formGroup: FormGroup;
  displayedColumns: string[] = ['accountName', 'monthYear', 'amount'];
  dataSource = [];
  userRole: string;
  constructor(private formBuilder: FormBuilder, private dataService: DataService, private authenticationService: AuthService) { }

  ngOnInit() {
    this.userRole = this.authenticationService.getLoggedUserRole();
    this.createForm();
    this.loadBalances();
  }

  createForm() {
    this.formGroup = this.formBuilder.group({
      date: [{ value: moment(), disabled: this.userRole !== 'Admin' }, Validators.required]
    });
  }

  chosenYearHandler(normalizedYear: Moment) {
    const ctrlValue = this.formGroup.get('date').value;
    ctrlValue.year(normalizedYear.year());
    this.formGroup.get('date').setValue(ctrlValue);
  }

  chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.formGroup.get('date').value;
    ctrlValue.month(normalizedMonth.month());
    this.formGroup.get('date').setValue(ctrlValue);
    datepicker.close();
    this.loadBalances();
  }

  loadBalances() {
    const selectedDate = this.formGroup.get('date').value;
    const date = selectedDate.clone().startOf('month');
    const req = {
      from: moment(date._d).format('YYYY-MM-DD')
    };
    console.log(req);
    this.dataService.getBalancesForMonth(req).then(res => {
      if (res) {
        this.dataSource = res;
      }
    });
  }
}
