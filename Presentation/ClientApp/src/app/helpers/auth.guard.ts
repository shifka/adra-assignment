// auth.guard.ts
import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
  constructor(private authenticationService: AuthService,private router: Router) {}

  canActivate() {

    if(!this.authenticationService.isUserLoggedIn())
    {
       this.router.navigate(['login']);
       return false;
    }

    return true;
  }
}