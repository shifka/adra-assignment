import { Injectable } from '@angular/core';
import { HttpService } from './shared/http.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import * as jwt_decode from 'jwt-decode';
import { UserToken } from '../models/userToken';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    private ROLE_CLAIM_TYPE: string = 'http://schemas.microsoft.com/ws/2008/06/identity/claims/role';
    private api_url = `${environment.api_base_url}`;
    private currentUserSubject: BehaviorSubject<UserToken>;
    public currentUser: Observable<UserToken>;
    constructor(private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<UserToken>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }
    public get currentUserValue(): UserToken {
        return this.currentUserSubject.value;
    }
    login(username: string, password: string) {

        return this.http.post<any>(`${this.api_url}auth/login`, { 'username': username, 'password': password })
            .pipe(map(result => {
                if (result && result.auth_token) {
                    localStorage.setItem('currentUser', JSON.stringify(result));
                    this.currentUserSubject.next(result);
                    return result;
                }
            }));
    }
    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }

    getLoggedUserRole(): any {
        try {
            var decoded = jwt_decode(this.currentUserSubject.value.auth_token);
            return decoded[this.ROLE_CLAIM_TYPE];
        } catch (Error) {
            return null;
        }
    }

    isUserLoggedIn(): any {
        return this.currentUserSubject.value && this.currentUserSubject.value.auth_token;
    }
}
