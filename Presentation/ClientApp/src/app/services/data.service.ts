import { Injectable } from '@angular/core';
import { HttpService } from './shared/http.service';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private httpService: HttpService) { }

  public getBalancesForMonth(req: any): Promise<any> {
    return this.httpService.post(`balance/get-balances-for-month`, req).toPromise();
  }

  public getBalancesForPeriod(req: any): Promise<any> {
    return this.httpService.post(`balance/get-balances-for-period`, req).toPromise();
  }
}
