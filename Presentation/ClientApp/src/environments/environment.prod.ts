export const environment = {
  production: true,
  api_base_url: 'https://adra-api.azurewebsites.net/api/'
};
