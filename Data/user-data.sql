INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'1', N'Admin', N'ADMIN', NULL)
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'2', N'User', N'USER', NULL)
GO
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount]) VALUES (N'457ce13a-c3c9-4513-ab5c-0a4ec30421db', N'admin', N'ADMIN', NULL, NULL, 0, N'AQAAAAEAACcQAAAAEDYZTXoPI0KNO8emTmniiDcRvtElWcZrb5vUkoMTN7uCbFcomTjlDr3zrGFbCEk4hg==', N'UHBK3EENJBILPHYLQLKNQBRQVAK762XM', N'c0de5e6c-07f5-4b2f-8916-4d98ca72128c', NULL, 0, 0, NULL, 1, 0)
GO
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount]) VALUES (N'add3a011-2b59-49a0-8f8d-27b2b1396f12', N'user', N'USER', NULL, NULL, 0, N'AQAAAAEAACcQAAAAEKXgIs+33rkPq9WgBGriI9wEs6iWHk3LjnpzM/fN8yOTitINuR59UmQPw2bvz4b6rA==', N'LETF6W2V5FXIDJC4GK5VEKEV57KXSTOW', N'd5ddc1d7-2472-453e-a6b8-2ede02e12bad', NULL, 0, 0, NULL, 1, 0)
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'457ce13a-c3c9-4513-ab5c-0a4ec30421db', N'1')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'add3a011-2b59-49a0-8f8d-27b2b1396f12', N'2')
GO
