﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdraAssignment.DTO
{
    public class CreateUserDto
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool isAdmin { get; set; }
    }
}
