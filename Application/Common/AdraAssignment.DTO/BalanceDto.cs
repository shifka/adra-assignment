﻿using System;
using System.Collections.Generic;

namespace AdraAssignment.DTO
{
    public class BalanceDto
    {
        public int Id { get; set; }
        public int AccountId { get; set; }
        public string AccountName { get; set; }
        public double Amount { get; set; }
        public DateTime MonthYear { get; set; }
        public AccountDto Account { get; set; }
    }

    public class BalanceRequestDto
    {
        public DateTime From { get; set; }
        public DateTime To { get; set; }
    }

    public class BalanceSeriesDto
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }

    public class BalanceRangeDto
    {
        public string Name { get; set; }
        public List<BalanceSeriesDto> Series { get; set; }
    }
}
