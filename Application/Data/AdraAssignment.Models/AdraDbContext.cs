﻿using AdraAssignment.Data.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;


namespace AdraAssignment.Data
{
    public class AdraDbContext : IdentityDbContext, IAdraDbContext
    {
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Balance> Balances { get; set; }

        public AdraDbContext(DbContextOptions options)
        : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Account>().HasData(new Account[]{
                new Account { Id = 1, Name = "R&D", Description = "R&D Department" },
                new Account { Id = 2, Name = "Canteen", Description = "Canteen" },
                new Account { Id = 3, Name = "CEO’s car", Description = "CEO" },
                new Account { Id = 4, Name = "Marketing", Description = "Marketing" },
                new Account { Id = 5, Name = "Parking fines", Description = "Parking" }
            });
        }
    }
}
