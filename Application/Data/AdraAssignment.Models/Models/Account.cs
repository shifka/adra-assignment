﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AdraAssignment.Data.Models
{
    public class Account
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
