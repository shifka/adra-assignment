﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AdraAssignment.Data.Models
{
    public class Balance
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("Account")]
        public int AccountId { get; set; }
        public virtual Account Account { get; set; }
        public DateTime MonthYear { get; set; }
        public double Amount { get; set; }
    }
}
