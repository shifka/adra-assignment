﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AdraAssignment.Data.Migrations
{
    public partial class InitialData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Accounts",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "R&D Department", "R&D" },
                    { 2, "Canteen", "Canteen" },
                    { 3, "CEO", "CEO’s car" },
                    { 4, "Marketing", "Marketing" },
                    { 5, "Parking", "Parking fines" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Accounts",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Accounts",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Accounts",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Accounts",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Accounts",
                keyColumn: "Id",
                keyValue: 5);
        }
    }
}
