﻿using AdraAssignment.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AdraAssignment.Data
{
    public interface IAdraDbContext: IDisposable
    {
        DbSet<Account> Accounts { get; set; }
        DbSet<Balance> Balances { get; set; }
        int SaveChanges();
    }
}
