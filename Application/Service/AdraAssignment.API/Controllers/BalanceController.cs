﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using AdraAssignment.Core.Services;
using AdraAssignment.DTO;
using ExcelDataReader;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace AdraAssignment.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class BalanceController : ControllerBase
    {
        private readonly IBalanceService balanceService;

        public BalanceController(IBalanceService _balanceService)
        {
            this.balanceService = _balanceService;
        }

        [Authorize(Roles = "Admin")]
        [HttpPost, DisableRequestSizeLimit]
        [Route("upload")]
        public IActionResult Upload()
        {
            try
            {
                var month = DateTime.Parse(Request.Form["date"]);
                var file = Request.Form.Files[0];
                var balances = new List<BalanceDto>();

                if (file.Length > 0)
                {
                    using (var reader = ExcelReaderFactory.CreateReader(file.OpenReadStream()))
                    {
                        reader.Read();
                        do
                        {
                            while (reader.Read())
                            {
                                var account = reader.GetString(0);
                                var balance = reader.GetDouble(1);
                                balances.Add(new BalanceDto { AccountName = account, Amount = balance, MonthYear = month });
                            }
                        } while (reader.NextResult());
                    }
                    balanceService.SaveUploadedExcelData(balances);
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            }
        }

        [Authorize]
        [HttpPost]
        [Route("get-balances-for-month")]
        public ActionResult GetBalancesForMonth([FromBody]BalanceRequestDto date)
        {
            return Ok(balanceService.GetBalancesForMonth(date.From));
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        [Route("get-balances-for-period")]
        public ActionResult GetBalancesForPeriod([FromBody]BalanceRequestDto date)
        {
            return Ok(balanceService.GetBalancesForPeriod(date.From, date.To));
        }
    }
}
