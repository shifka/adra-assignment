﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdraAssignment.Data.Models.Identity;
using AdraAssignment.DTO;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace AdraAssignment.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AccountController : ControllerBase
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IMapper _mapper;

        public AccountController(UserManager<IdentityUser> userManager, IMapper mapper)
        {
            _userManager = userManager;
            _mapper = mapper;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]CreateUserDto model)
        {
            var userIdentity = _mapper.Map<IdentityUser>(model);
            var result = await _userManager.CreateAsync(userIdentity, model.Password);
            
            if (!result.Succeeded)
            {
                return BadRequest();
            }

            var user = await _userManager.FindByNameAsync(model.Username);
            await _userManager.AddToRoleAsync(user, model.isAdmin ? "Admin" : "User");

            return new OkResult();
        }
    }
}
