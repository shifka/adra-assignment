﻿using AdraAssignment.Data;
using AdraAssignment.Data.Models;
using AdraAssignment.DTO;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdraAssignment.Core.Services
{
    public class BalanceService : IBalanceService
    {
        IAdraDbContext adraDbContext;
        private readonly IMapper mapper;

        public BalanceService(IAdraDbContext _adraDbContext, IMapper _mapper)
        {
            adraDbContext = _adraDbContext;
            mapper = _mapper;
        }
        public void SaveUploadedExcelData(List<BalanceDto> balances)
        {
            var accounts = adraDbContext.Accounts.ToList();
            if (balances.Any())
            {
                var previousBalances = adraDbContext.Balances.Where(b => b.MonthYear == balances.First().MonthYear);
                if (previousBalances.Any())
                {
                    adraDbContext.Balances.RemoveRange(previousBalances);
                }
                
                foreach (var balance in balances)
                {
                    adraDbContext.Balances.Add(new Balance
                    {
                        AccountId = accounts.FirstOrDefault(a => a.Name == balance.AccountName).Id,
                        Amount = balance.Amount,
                        MonthYear = balance.MonthYear
                    });
                }
                adraDbContext.SaveChanges();
            }
        }

        public List<BalanceDto> GetBalancesForMonth(DateTime date)
        {
            var balances = adraDbContext.Balances.Include(b => b.Account).Where(b => b.MonthYear.Month == date.Month && b.MonthYear.Year == date.Year).ToList();
            return mapper.Map<List<BalanceDto>>(balances);
        }

        public List<BalanceRangeDto> GetBalancesForPeriod(DateTime from, DateTime to)
        {
            var accounts = adraDbContext.Accounts.ToList();
            var balances = adraDbContext.Balances.Where(b => b.MonthYear >= from && b.MonthYear <= to).OrderBy(b => b.MonthYear);
            var result = new List<BalanceRangeDto>();
            foreach (var account in accounts)
            {
                result.Add(new BalanceRangeDto
                {
                    Name = account.Name,
                    Series = balances.Where(b => b.AccountId == account.Id).Select(b => new BalanceSeriesDto { Name = String.Format("{0:MMM yyyy}", b.MonthYear), Value = b.Amount.ToString() }).ToList()
                });
            }
            return result;
        }
    }
}
