﻿using AdraAssignment.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdraAssignment.Core.Services
{
    public interface IBalanceService
    {
        void SaveUploadedExcelData(List<BalanceDto> balances);
        List<BalanceDto> GetBalancesForMonth(DateTime date);
        List<BalanceRangeDto> GetBalancesForPeriod(DateTime from, DateTime to);

    }
}
