﻿using AdraAssignment.Data.Models;
using AdraAssignment.Data.Models.Identity;
using AdraAssignment.DTO;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdraAssignment.Core.AutoMapperConf
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Balance, BalanceDto>()
                .ForMember(data => data.AccountName, source => source.MapFrom(i => i.Account.Name));
            CreateMap<BalanceDto, Balance>();

            CreateMap<Account, AccountDto>();
            CreateMap<AccountDto, Account>();

            CreateMap<CreateUserDto, IdentityUser>();
        }
    }
}
