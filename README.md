#To run API:

##Developed on .NET Core 3.1 - Requires VS2019

Change connection string in appsettings.json to your local SQL server and run the application

Database will be automatically migrated

Run the 'user-data.sql' script from the 'Data' folder to create initial users and roles mapping

To create new users (specify isAdmin true/false accordingly):

https://{application-url}/api/account
{
	"userName":"newadmin",
	"password":"welcome@123",
	"isAdmin":true
}


#To run Angular app:

Go to Presentation -> ClientApp

Open CMD and execute following commands:

    npm install
    npm run start
	
Browse application on http://localhost:4200/


